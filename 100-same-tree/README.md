# 100-same-tree

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(h) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Implemented Optimized Solutions by using DFS.
    - Improved code a bit after seeing Solution.
    - Time Complexity would be O(min(n,m), n,m = no of nodes
    - Space Complexity would be O(min(h,H), h,H = height of tree
