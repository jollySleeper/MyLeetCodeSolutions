# 102-level-order-traversal-of-bt

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(h)/O(n) Space Complexity Using DFS and BFS.

### JavaScript

### Rust
 
## Notes
- Implemented Optimized Solutions by using BFS & DFS.
    - Time Complexity would be O(n), n = no of nodes
    - Space Complexity would be O(h), h = height of tree for DFS
    - Space Complexity would be O(n), n = height of tree for BFS
    - Almost implement code by learning BFS but had to refer solutions section and was amazed by seeing a DFS approach.
    - Refer this [solution](https://leetcode.com/problems/binary-tree-level-order-traversal/solutions/33450/java-solution-with-a-queue-used/comments/32165) for BFS.
    - And this [solution](https://leetcode.com/problems/binary-tree-level-order-traversal/solutions/33445/java-solution-using-dfs) for DFS.
