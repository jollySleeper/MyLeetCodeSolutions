import java.util.List;
import java.util.Queue;
import java.util.ArrayList;
import java.util.LinkedList;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        this.val = x;
    }
}

class Solution {

    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }

        // return this.levelOrderBFS(root);
        return this.levelOrderDFS(new ArrayList<List<Integer>>(), root, 0);
    }

    public List<List<Integer>> levelOrderBFS(TreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }

        List<List<Integer>> ans = new ArrayList<>();
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);

        while (!q.isEmpty()) {
            List<Integer> level = new ArrayList<>();
            int queueSize = q.size();
            for (int i = 0; i < queueSize; i++) {
                TreeNode node = q.poll();
                level.add(node.val);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
            ans.add(level);
        }

        return ans;
    }

    public List<List<Integer>> levelOrderDFS(List<List<Integer>> ans, TreeNode root, int height) {
        if (root == null) {
            return new ArrayList<List<Integer>>();
        }

        if (ans.size() <= height) {
            ans.add(new ArrayList<Integer>());
        }

        ans.get(height).add(root.val);
        levelOrderDFS(ans, root.left, height + 1);
        levelOrderDFS(ans, root.right, height + 1);

        return ans;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);

        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);

        Solution solutionObject = new Solution();
        System.out.println("Level Order Traversal of Tree is = " + solutionObject.levelOrder(root));
    }
}
