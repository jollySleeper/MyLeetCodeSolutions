# 104-height-of-binary-tree

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(h) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Implemented two Solutions by using DFS.
    - For more Optimized Solution referred this [solution](https://leetcode.com/problems/maximum-depth-of-binary-tree/solutions/1770060/c-recursive-dfs-example-dry-run-well-explained/comments/1702304).
    - Solution I implemented had some hack to find maximum value.
    - Time Complexity would be O(n), n = no of nodes
    - Space Complexity would be O(h), h = height of binary tree
