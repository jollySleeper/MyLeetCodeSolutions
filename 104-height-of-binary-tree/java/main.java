class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode (int x) {
        this.val = x;
    }

}

class Solution {

    int max = 1;
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return -1;   
        }
        if (root.right == null && root.left == null) {
            return 1;   
        }
       
        // Method 1: Self Implementation
        // return dfs(root, this.max);
        // Method 2: After Seeing Solution
        return findDepth(root, 0);
    }

    public int dfs (TreeNode root, int max) {
        if (root.left != null) {
            this.max = dfs(root.left, max + 1);
        } 
        if (root.right != null) {
            this.max = dfs(root.right, max + 1);
        }

        return Math.max(max, this.max);
    }

    public int findDepth(TreeNode root, int depth) {
        if (root == null) {
            return depth;
        }
        int left = findDepth(root.right, depth + 1);
        int right = findDepth(root.left, depth + 1);

        return Math.max(left, right);
    }

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(3);
        tree.left = new TreeNode(9);
        tree.right = new TreeNode(20);

        tree.right.left = new TreeNode(15);
        tree.right.right = new TreeNode(7);

        Solution solutionObject = new Solution();
        System.out.println("Height of Tree is = " + solutionObject.maxDepth(tree));       
    }
}
