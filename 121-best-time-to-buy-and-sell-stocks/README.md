# 121-best-time-to-buy-and-sell-stocks

## Solutions

### C++

### Go

### Java
Implemented Brute Force O(n^2) Solution, Giving TLE in LeetCode

### JavaScript

### Rust
 
## Notes
Saw the Solution for O(n) Time Complexity. 2 approcahes were mentioned.
> Practice Both Solutions, Kadane won't be common but good to have.
1. [Two Pointer Method](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/solutions/1735550/python-javascript-easy-solution-with-very-clear-explanation)
2. [Kadane's Algo](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/solutions/39038/kadane-s-algorithm-since-no-one-has-mentioned-about-this-so-far-in-case-if-interviewer-twists-the-input)
