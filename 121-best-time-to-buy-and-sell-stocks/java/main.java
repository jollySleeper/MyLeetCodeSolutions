class Solution {
    
    public int maxProfit(int[] prices) {
        if (prices.length < 2) {
            return 0;
        }

        int ans = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                int profit = prices[j] - prices[i];
                // System.out.println("Profit " + profit);
                ans = Math.max(ans, profit);
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        int[] priceArr = {7,6,4,3,1};
        Solution solutionObject = new Solution();;

        System.out.println("Max Profit = " + solutionObject.maxProfit(priceArr));       
    }
}
