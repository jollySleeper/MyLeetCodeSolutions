# 141-linked-list-cycle

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(1) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Skipped Brute Force Approach which can be done using HashMap:
    - Nodes can be inserted in the hash map to check if they are visited.
    - Time Complexity would be O(n)
    - Space Complexity would be O(n)
- Implemented a Optimized Solution by using TortoiseHair Method.
    - In this method one pointer moves doubly fast than the other pointer.
    - Loop traverse n/2 + 1 times in worst case.
    - Time Complexity would be O(n)
    - Space Complexity would be O(1)

