class ListNode {
    int val;
    ListNode next;

    ListNode (int x) {
        this.val = x;
        this.next = null;
    }
}

class Solution {

    public void printLinkedList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
        System.out.print("null");
    }


    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }

        ListNode slow = head;
        ListNode fast = head;
        while (
            slow != null
            && slow.next != null
            && fast != null
            && fast.next != null
            && fast.next.next != null
        ) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(3);
        head.next = new ListNode(2);
        head.next.next = new ListNode(0);
        head.next.next.next = new ListNode(-4);
        // head.next = new ListNode(2);
       
        ListNode start = head;
        int pos = 1, index = 0;
        ListNode tmp = null;
        while (head.next != null) {
            if (index == pos) {
                tmp = head;
            }
            head = head.next;
            index += 1;
        }
        // Last Elemnet
        head.next = tmp;

        Solution solutionObject = new Solution();
        // solutionObject.printLinkedList(start);
        System.out.println("Cycle in Linked List = " + solutionObject.hasCycle(start));       
    }
}
