# 143-reorder-list

## Solutions

### C++

### Go

### Java
Solved with Time Complexity O(n) & Space Complexity O(n) for Reversing.

### JavaScript

### Rust
 
## Notes
- Skipped Brute Force Approach which can be done using an array.
    - Time Complexity would be O(n)
    - Space Complexity would be O(n)
- Imlemented the same Complexity solution using only Linked List.
- This solution can be optimized more by reversing only till middle.
    - Refer this [solution](https://leetcode.com/problems/reorder-list/solutions/44992/java-solution-with-3-steps).
