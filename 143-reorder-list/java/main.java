class ListNode {
    int val;
    ListNode next;

    ListNode() {}

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

class Solution {

    public void printLinkedList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
        System.out.print("null");
    }

    public ListNode copyLinkedList(ListNode head) {
        if (head == null) return null;
        ListNode copyHead = new ListNode(head.val);
        ListNode copy = copyHead;
        head = head.next;

        while (head != null) {
            copy.next = new ListNode(head.val);
            copy = copy.next;
            head = head.next;
        }

        return copyHead;
    }

    int len = 0;
    public ListNode reverseLinkedList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;

        while (current != null) {
            ListNode tmp = current.next;
            current.next = prev;
            prev = current;
            current = tmp;
            this.len++;
        }

        return prev;
    }

    public void reorderList(ListNode head) {
        ListNode normal = copyLinkedList(head);
        ListNode reverse = reverseLinkedList(head);

        ListNode current = head;
        normal = normal.next;
        int i = 1;
        while (i < this.len && (normal != null || reverse != null)) {
            if (i % 2 == 0 && normal != null) {
                current.next = normal;
                current = current.next;
                normal = normal.next;
            } else if (reverse != null) {
                current.next = reverse;
                current = current.next;
                reverse = reverse.next;
            }
            current.next = null;
            i += 1;
        }
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        // head.next.next.next.next = new ListNode(5);
        // head.next.next.next.next.next = new ListNode(6);

        Solution solutionObject = new Solution();
        System.out.print("Reordered List is = ");
        solutionObject.reorderList(head);
        solutionObject.printLinkedList(head);
    }
}
