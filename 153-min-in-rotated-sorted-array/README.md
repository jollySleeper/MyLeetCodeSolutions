# 153-min-in-rotated-sorted-array

## Solutions

### C++

### Go

### Java
Solved Using Slight Modification in Binary Search Algo which means Time Complexity is O(logn)

### JavaScript

### Rust
 
## Notes
Initailly Referred to this [Video](https://youtu.be/nhEMDKMB44g) for solving this problem.
Referred to [Solution](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/solutions/48493/compact-and-clean-c-solution) for Writting If Conditions.
