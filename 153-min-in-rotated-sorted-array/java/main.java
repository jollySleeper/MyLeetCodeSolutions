class Solution {

    public int findMin(int[] nums) {
        if (nums.length < 1) {
            return -1;
        }
        if (nums.length == 1) {
            return nums[0];
        }

        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
            if (nums[left] < nums[right]) {
                // The Smallest Element Will always be Left in this Case
                return nums[left];
            } 

            // When nums[left] > nums[right]
            // Checking for Values with mid
            // If mid is greater than left then min would be in between mid to right
            // If mid is smaller than right then min would be between left to mid
            int mid = (int) Math.floor((left + right)/2);
            if (nums[mid] >= nums[left]) {
                left = mid + 1;
            } else {
                right = mid;
            }

        }

        return nums[left];
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int[] nums = {3,4,5,1,2};
        System.out.println("Minimum in Sorted Array is = " + solutionObject.findMin(nums)); 
    }
}
