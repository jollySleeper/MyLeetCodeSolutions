# 226-invert-binary-tree

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(h) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Implemented Optimized Solutions by using DFS.
    - Time Complexity would be O(n), n = no of nodes
    - Space Complexity would be O(h,), h = height of tree
