# 229-majority-element-2

## Solutions

### C++

### Go

### Java
Solved after watching the Solution Video with O(n) Time Complexity & O(1) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Brute for Approach can was skipped, can be done with 2 for loops i.e O(n^2) Time Complexity.
- A more optimal Approach could be to use a HashMap for Counting Frequency.
    - Time Complexity would be O(n).
    - Space Complexity would be O(n).
- The Most optimal way for this is Boyer-Moore Algorithm with O(1) Space Complexity.
    - Refered this [video](https://youtu.be/vwZj1K0e9U8) for Solving in Optimized Way.
    - This [solution](https://leetcode.com/problems/majority-element-ii/solutions/4131226/99-7-hashmap-boyer-moore-majority-voting-explained-intuition) can be used for revision.
    - Time Complexity would be O(n).
    - Space Complexity would be O(1).
    - Solution is the extended Approach of this Algorithm. 
    - This Algo Observers that there would be only 2 elements having freq greater than n/3.
    - So, keeping count of 2 candidates.
