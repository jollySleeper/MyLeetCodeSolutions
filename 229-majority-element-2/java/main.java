import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

class Solution {

    public List<Integer> majorityElement(int[] nums) {
        if (nums.length < 1) {
            return Arrays.asList(-1);
        }

        int num1 = 0, num2 = 0;
        int countOfNum1 = 0;
        int countOfNum2 = 0;
        for (int i = 0; i < nums.length; i++) {
            if (countOfNum1 == 0 && num2 != nums[i]) {
                countOfNum1++;
                num1 = nums[i];
            } else if (countOfNum2 == 0 && num1 != nums[i]) {
                countOfNum2++;
                num2 = nums[i];
            } else if (nums[i] == num1) {
                countOfNum1++;
            } else if (nums[i] == num2) {
                countOfNum2++;
            } else {
                countOfNum1--;
                countOfNum2--;
            }
        }

        List<Integer> majorityElements = new ArrayList<>();
        countOfNum1 = 0;
        countOfNum2 = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == num1) {
                countOfNum1++;
            } else if (nums[i] == num2) {
                countOfNum2++;
            }
        } 

        int majorityMark = (int) Math.floor(nums.length / 3);
        if (countOfNum1 > majorityMark) {
            majorityElements.add(num1);
        }
        if (countOfNum2 > majorityMark) {
            majorityElements.add(num2);
        }

        return majorityElements;
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int[] nums = {3,2,3};
        System.out.println("Majority Elements of Array are = " + solutionObject.majorityElement(nums));       
    }
}
