# 238-product-of-array-except-self

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(n) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Skipped Brute Force Approach which can be done with 2 loops and skipped the element with same index.
    - Time Complexity would be O(n^2)
    - Space Complexity would be O(1)
- Implemented a Optimized Solution by Intuition by using 2 Pointer Method.
    - Used two array(prev & next) to keep track of product.
    - Prev pointer was moving from left to right.
    - Next pointer was moving from right to left.
    - Used one more loop to Calculate the Product `prev[i] * next[i]`
    - Time Complexity would be O(n)
    - Space Complexity would be O(n)
- The most Optimal way of doing this is by Eliminating the Prev & Next Array and directly Storing Product in Result Array
    - Referred to this [solution](https://leetcode.com/problems/product-of-array-except-self/solutions/1342916/3-minute-read-mimicking-an-interview) for checking optimal solution.
    - Time Complexity would be O(n)
    - Space Complexity would be O(1)

