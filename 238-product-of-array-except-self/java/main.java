import java.util.Arrays;

import java.util.Arrays;

class Solution {

    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        if (n < 1) {
            return new int[]{-1};
        }
        if (n == 1) {
            return nums;
        }
        
        int[] prevProduct = new int[n];
        // Arrays.fill(prevProduct, 1);
        int[] nextProduct = new int[n];
        // Arrays.fill(nextProduct, 1);
        
        prevProduct[0] = 1;
        nextProduct[n - 1] = 1;
        for (int i = 1; i < n; i++) {
            prevProduct[i] = prevProduct[i - 1] * nums[i - 1];
            nextProduct[n - 1 - i] = nextProduct[n - i] * nums[n - i];
        }

        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            result[i] = prevProduct[i] * nextProduct[i];
        }
        
        return result;
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int[] nums = {1,2,3,4};
        System.out.println(
            "Product of Array Except Self is = "
                + Arrays.toString(solutionObject.productExceptSelf(nums))
        );       
    }
}
