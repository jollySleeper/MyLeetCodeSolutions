#include <bits/stdc++.h>
#include <climits>
#include <string>
#include <unordered_set>

using namespace std;

class Solution {
public:
    /**
     * Time Complexity: O(n^2)
     * Space Complexity: O(n)
     * n = lenght of string
     *
     * Making every possible substring with
     * unique chars with 2nd loop
     */
    int lengthOfLongestSubstring(string s) {
        if (s.length() < 1) {
            return s.length();
        }

        int max = 1;
        for (int i = 0; i < s.length(); i++) {
            int counter = 1;
            unordered_map<char, bool> visited;
            visited[s[i]] = true;

            for (int j = i + 1; j < s.length(); j++) {
                if (visited[s[j]]) {
                    break;
                }

                visited[s[j]] = true;
                ++counter;

                if (max < counter) {
                    max = counter;
                }
            }
        }

        return max;
    }
};

int main() {
    Solution solutionObject;
    string str = "pwwkew";
    cout << "Lenght of Longest Substring of String = " << str;
    cout << "is = " << solutionObject.lengthOfLongestSubstring(str);

    return 0;
}
