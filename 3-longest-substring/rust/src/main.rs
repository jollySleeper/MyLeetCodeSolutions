use std::collections::HashMap;

pub struct Solution;

/**
 * Time Complexity: O(n)
 * Space Complexity: O(n)
 * n = lenght of string
 *
 * TODO: Can Optimize this Code More
 * As Currently Using i++ to bring it to towards j.
 */
impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut i = 0;
        let mut j = 1;
        let mut max = if s.len() > 0 {
            1
        } else {
            0;
            return 0;
        };
        let mut map: HashMap<char, bool> = HashMap::new();
        map.insert(s.chars().next().unwrap(), true);

        while i < j && i < s.len() && j < s.len() {
            if map.contains_key(&s.chars().nth(j).unwrap()) == false
                || !map.get(&s.chars().nth(j).unwrap()).unwrap()
            {
                map.insert(s.chars().nth(j).unwrap(), true);
                let diff = j - i + 1;
                if diff > max {
                    max = diff;
                }
                j += 1;
            } else {
                map.insert(s.chars().nth(i).unwrap(), false);
                i += 1;
            }

            if i >= j {
                map.insert(s.chars().nth(i).unwrap(), true);
                j = i + 1;
            }
        }

        return max.try_into().unwrap();
    }
}

fn main() {
    let str: String = "".to_string();
    println!("String = {:?}", str);
    println!(
        "Lenght of Longest Substring of String is = {:?}",
        Solution::length_of_longest_substring(str)
    );
}
