pub struct Solution;

impl Solution {
    pub fn spiral_order(matrix: Vec<Vec<i32>>) -> Vec<i32> {
        let result = Vec::new();
        let mut transpose_of_matrix: Vec<Vec<i32>> = Vec::new();
        for _ in 0..matrix.len() {
            transpose_of_matrix.push(vec![0; matrix[0].len()]);
        }

        // Transponse Matrix: Row to Column, Column to Row
        for i in 0..matrix.len() {
            for j in 0..matrix[0].len() {
                transpose_of_matrix[i][j] = matrix[j][i];
                transpose_of_matrix[j][i] = matrix[i][j];
            }
        }
        println!("Transpose of Matrix = {:?}", transpose_of_matrix);

        return result;
    }
}

fn main() {
    let nums = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
    println!(
        "Spiral Traversal of Nums {:?} is {:?}",
        nums,
        Solution::spiral_order(nums.clone())
    );
}
