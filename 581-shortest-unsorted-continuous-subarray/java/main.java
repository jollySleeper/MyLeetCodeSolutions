class Solution {

    public int findUnsortedSubarray(int[] nums) {
        int len = nums.length;
        if (len < 1) {
            return -1;
        }
        if (len == 1) {
            return 0;
        }

        int minIndex = Integer.MAX_VALUE;
        int maxIndex = Integer.MIN_VALUE;
        int maxNum = nums[0];
        for (int i = 1; i < len; i++) {
            if (nums[i] < nums[i - 1] || nums[i] < maxNum) {
                minIndex = Math.min(minIndex, i);
                maxIndex = Math.max(maxIndex, i);

                for (int j = i - 1; j >= 0; j--) {
                    if (nums[i] >= nums[j]) {
                        break;
                    }
                    minIndex = Math.min(minIndex, j);
                }
            }
            maxNum = Math.max(maxNum, nums[i]);
        }

        if (minIndex != Integer.MAX_VALUE && maxIndex != Integer.MIN_VALUE) {
            return maxIndex - minIndex + 1;
        }

        return 0;   
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int[] nums = {1,3,2,2,2};

        System.out.println(
            "Unsorted Subarray is of Length = " + solutionObject.findUnsortedSubarray(nums)
        );       
    }
}
