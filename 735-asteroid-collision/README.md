# 735-asteroid-collision

## Solutions

### C++

### Go

### Java

### JavaScript

### Rust
 
## Notes
The Question considers that all the asteroids will originate from the same point.
So, if the left is originating before right it won't collide
Once the right asteroid starts its course then any left moving asteroid can collide with it.
This questions also condiers the order of asteroids.
    for example 5,-4,3 will result in 5,3 as -4 will collide with 5 and burst.
    but 5, 3,-4 will resutl in 5 as -4 will eat 3 and then -4 will burst to 5.
In my first attempt I didn't consider order and assumed that left is starting from left side and right is starting from rigth side.
