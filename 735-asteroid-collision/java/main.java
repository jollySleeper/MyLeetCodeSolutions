import java.util.ArrayList;
import java.util.Arrays;

class Solution {

    public int[] asteroidCollision(int[] asteroids) {
        int len = asteroids.length;
        if (len < 1) {
            return new int[] {-1};
        }
        
        ArrayList<Integer> ans = new ArrayList<>();
        ArrayList<Integer> rightMovingAsteroids = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            if (asteroids[i] < 0) {
                if (rightMovingAsteroids.size() < 1) {
                    ans.add(asteroids[i]);
                    continue;
                }
                for (int j = rightMovingAsteroids.size() -1 ; j>=0 ;j--) {
                    if (asteroids[i] * -1 < rightMovingAsteroids.get(j)) {
                        break;
                    }else if (asteroids[i] * -1 == rightMovingAsteroids.get(j)) {
                        rightMovingAsteroids.remove(j);
                        break;
                    }else if (asteroids[i] * -1 > rightMovingAsteroids.get(j)) {
                        rightMovingAsteroids.remove(j);
                        if(rightMovingAsteroids.size() < 1) {
                            ans.add(asteroids[i]);
                        }
                    }
                }
            }
            
            if (asteroids[i] > 0) {
                rightMovingAsteroids.add(asteroids[i]);
            }
        }
        ans.addAll(rightMovingAsteroids);

        return ans.stream().mapToInt(i -> i).toArray();
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int[] asteroids = {5,10,-5};
        System.out.println(
            "Asteroid which Survived are = "
            + Arrays.toString(solutionObject.asteroidCollision(asteroids))
        );       
    }
}
