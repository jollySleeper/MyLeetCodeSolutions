use std::usize;

pub struct Solution;

impl Solution {
    pub fn exist(board: Vec<Vec<char>>, word: String) -> bool {
        let first_char = word.chars().next().unwrap();
        for i in 0..board.len() {
            for j in 0..board[0].len() {
                if board[i][j] == first_char {
                    if Solution::recursive_search(i, j, board.clone(), word.clone(), 1) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    pub fn recursive_search(
        x: usize,
        y: usize,
        board: Vec<Vec<char>>,
        word: String,
        found: usize,
    ) -> bool {
        if found >= word.len() {
            return true;
        }

        let mut exists = false;
        let char_to_find = word.chars().nth(found).unwrap();

        if x < board.len() - 1 && board[x + 1][y] == char_to_find {
            exists = Solution::recursive_search(x + 1, y, board.clone(), word.clone(), found + 1);
        }
        if y < board[0].len() - 1 && board[x][y + 1] == char_to_find {
            exists = Solution::recursive_search(x, y + 1, board.clone(), word.clone(), found + 1);
        }
        if y > 0 && board[x][y - 1] == char_to_find {
            exists = Solution::recursive_search(x, y - 1, board.clone(), word.clone(), found + 1);
        }
        if x > 0 && board[x - 1][y] == char_to_find {
            exists = Solution::recursive_search(x - 1, y, board.clone(), word.clone(), found + 1);
        }

        println!("Exists = {}, Found = {}", exists, found);
        return exists;
    }
}

fn main() {
    let matrix = Vec::from([
        Vec::from(['A', 'B', 'C', 'E']),
        Vec::from(['S', 'F', 'C', 'S']),
        Vec::from(['A', 'D', 'E', 'E']),
    ]);
    let word = "SEE".to_string();
    println!("Finding Word = {}, in Board = {:?}", word, matrix);
    println!("Word found = {}", Solution::exist(matrix, word));
}
