# 875-koko-eating-bananas

## Solutions

### C++

### Go

### Java
Solved with Time Complexity of O(n*log(max(piles[0..n]))) & O(1) Space Complexity using Binary Search on Answers.

### JavaScript

### Rust
 
## Notes
- Skipped Brute Force Approach which can be done with 2 loops and calculating total hours starting from 1 to asnwer.
    - Time Complexity would be O(n^2)
    - Space Complexity would be O(1)
- Implemented the most Optimal way of doing this is by using Binary Search on Answers
    - Referred to this [video](https://youtu.be/qyfekrNni90) for learning this method.
    - Referred to this [solution](https://leetcode.com/problems/koko-eating-bananas/solutions/1703687/java-c-a-very-very-well-detailed-explanation).
    - Time Complexity would be O(n*log(max(piles[0..n])))
    - Space Complexity would be O(1)

