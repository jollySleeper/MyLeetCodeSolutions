import java.util.Arrays;

class Solution {

    public int minEatingSpeed(int[] piles, int h) {
        int hours = h;
        int len = piles.length;
        if (len < 1 || hours < len) {
            return -1;
        }

        int right = piles[0];
        for (int i = 1; i < len; i++) {
            right = Math.max(right, piles[i]);
        }

        if (len == hours) {
            return right;
        }
        
        int left = 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            // System.out.println("Left = " + left + " Mid = " + mid + " Right = " + right);
            if (totalHoursOnRate(piles, mid) <= hours) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return left;
    }

    public long totalHoursOnRate(int[] baskets, int rate) {
        long totalHours = 0;
        for (int i = 0; i < baskets.length; i++) {
            // Math.ceil is not rounding up numbers like 2.000000001 to 3 for some reason.
            // float ratePerHour = (float) baskets[i] / rate;
            // totalHours += (int) Math.ceil(ratePerHour);
            
            // Using Mod Operator Instead
            totalHours += (baskets[i] / rate);
            if (baskets[i] % rate != 0) {
                totalHours++;
            }
        }
       
        return totalHours;
    }

    public static void main(String[] args) {
        Solution solutionObject = new Solution();
        int hours = 312884469;
        int[] baskets = {312884470};
        System.out.println("The minEatingSpeed is = " + solutionObject.minEatingSpeed(baskets, hours));       
    }
}
