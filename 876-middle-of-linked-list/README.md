# 876-middle-of-linked-list

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(1) Space Complexity.

### JavaScript

### Rust
 
## Notes
- Skipped Brute Force Approach which can be done with 2 loops and no extra space:
    - 1st to iterate through the linked list and finding the count of elements.
    - Then finding the the mid by n/2
    - 2nd loop to traverse to n/2 to find the value.
    - Time Complexity would be O(n)
    - Space Complexity would be O(1)
    - A more Brute Force Approach would be to store the values of linked list in an array and returning the n/2 element.
- Implemented a Optimized Solution by using TortoiseHair Method.
    - In this method one pointer moves doubly fast than the other pointer.
    - Loop traverse n/2 + 1 times in worst case.
    - Time Complexity would be O(n)
    - Space Complexity would be O(1)
