class ListNode {
    int val;
    ListNode next;

    ListNode(int val) {
        this.val = val;
    }
}

class Solution {

    public void printLinedList(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
        System.out.print("null");
    }

    public ListNode middleNode(ListNode head) {
        ListNode linear = head;
        ListNode doubleLinear = head;
        // while (doubleLinear != null || doubleLinear.next != null) {
        // while (doubleLinear != null) {
        while (true) {
            if (doubleLinear == null || doubleLinear.next == null ) {
                break;
            }
            linear = linear.next;
            doubleLinear = doubleLinear.next.next;
        }

        return linear;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        // head.next.next.next.next.next = new ListNode(6);

        Solution solutionObject = new Solution();
        ListNode curr = solutionObject.middleNode(head);
        System.out.println("Middle Node Val = " + curr.val);       
    }
}
