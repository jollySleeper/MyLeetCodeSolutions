# 98-validate-binary-search-tree

## Solutions

### C++

### Go

### Java
Solved with O(n) Time Complexity & O(n) Space Complexity Using DFS.

### JavaScript

### Rust
 
## Notes
- Implemented Solution using DFS.
    - Time Complexity would be O(n), n = no of nodes
    - Space Complexity would be O(n), n = no of nodes
    - Used a list to store Inorder Traversal of Tree.
    - If it's BST the list will be sorted by default.
    - Space can be optimized more, realized this from this [solution](https://leetcode.com/problems/validate-binary-search-tree/solutions/32112/learn-one-iterative-inorder-traversal-apply-it-to-multiple-tree-questions-java-solution/comments/145081).
