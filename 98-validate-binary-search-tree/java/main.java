import java.util.ArrayList;

import com.sun.source.tree.Tree;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        this.val = x;
    }
}

class Solution {

    public boolean isValidBSTTry(TreeNode root) {
        if (root == null) {
            return true;
        }

        boolean left = true;
        boolean right = true;
        if (root.left != null) {
            if (root.val > root.left.val) {
                left = true && isValidBSTTry(root.left);
            } else {
                return false;
            }
        }
        if (root.right != null) {
            if (root.val < root.right.val) {
                right = true && isValidBSTTry(root.right);
            } else {
                return false;
            }
        }

        return left && right;
    }
    
    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }

        ArrayList<Integer> array = this.inoderTraversal(root, new ArrayList<>());
        for(int i = 1; i < array.size(); i++) {
            if (array.get(i - 1) >= array.get(i)) {
                return false;
            }
        }

        return true;
    }

    public ArrayList<Integer> inoderTraversal(TreeNode root, ArrayList<Integer> array) {
        if (root.left != null) {
            inoderTraversal(root.left, array);
        }
        array.add(root.val);
        if (root.right != null) {
            inoderTraversal(root.right, array);
        }

        return array;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(1);
        root.right = new TreeNode(7);

        root.right.left = new TreeNode(4);
        root.right.right = new TreeNode(8);

        Solution solutionObject = new Solution();
        System.out.println("PreOrder Traversal = "); 
        // solutionObject.inoderTraversal(root);
        System.out.println("The Tree is BST = " + solutionObject.isValidBST(root));
    }
}
