# leetcode Initialized

## Agenda

### Optimize
- [ ] 121 : [Buy Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock)
- [ ] 238 : [Product of Array Except Self](https://leetcode.com/problems/product-of-array-except-self)
- [ ] 143 : [Reorder List](https://leetcode.com/problems/reorder-list)

### Revise/Challenge (Similar Problems)
- [ ] 33 : [Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array)
    > Have done find the min in Sorted Array this question is a bit different.
- [ ] 169 : [Majority Element](https://leetcode.com/problems/majority-element)
    > Practice Boyer-Moore Algorithm
- [ ] 2226 : [Maximum Candies Allocated to K Children]()
    > Have done Similar question of Koko, try this one for practice
- [ ] 19 : [Remove Nth Node From End of List](https://leetcode.com/problems/remove-nth-node-from-end-of-list)
    > Referred to this [solution])(https://leetcode.com/problems/remove-nth-node-from-end-of-list/solutions/8804/simple-java-solution-in-one-pass/comments/9950) for optimized solution, will need to practice it.

## How To Run Solutions Locally

### Java
- From Question Folder run `javac java/main.java -d java/target` for compiling.
- For running the program run `java -cp java/target Solution` as class name is Solution.
- For Direct Result Run 
    `javac java/main.java -d java/target && java -cp java/target Solution`

### Go

### JS

### C++
